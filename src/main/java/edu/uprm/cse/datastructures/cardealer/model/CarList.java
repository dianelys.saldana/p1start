package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	// Instance variable
	private static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<>(new CarComparator());
	
	// Returns instance of the list
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return cList;
	}
	
	public static void resetCars() {
		cList = new CircularSortedDoublyLinkedList<>(new CarComparator());
	}
}
