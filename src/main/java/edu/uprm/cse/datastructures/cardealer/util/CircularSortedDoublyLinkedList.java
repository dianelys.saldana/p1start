package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	// Instance Variables
	private Node<E> header;
	private int size = 0;
	private Comparator<E> comparator;

	// Constructor
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.header = new Node<E>(null, this.header, this.header);
		this.comparator = comp;
	}

	// Returns new iterator
	@Override
	public Iterator<E> iterator() {
		return new MyIterator();
	}

	// Adds element to the list; returns true if element is successfully added
	@Override
	public boolean add(E obj) {
		Node<E> newNode = new Node<E>(obj, null, null); // Node to add in list
		Node<E> curr = header.getNext();
		Node<E> node;
		
		if(this.isEmpty()) { // If List is empty -> adds element after next of header and before previous node of header
			newNode = new Node<E>(obj, this.header, this.header);
			header.setNext(newNode);
			header.setPrev(newNode);
			this.size++;
			return true;
		}
		
		while(curr != this.header) { 
			if(this.comparator.compare(obj, curr.getElement()) < 0) { // Verifies if new element must be placed before current element
				node = new Node<E>(obj, curr.getPrev(), curr);
				node.getPrev().setNext(node);
				node.getNext().setPrev(node);
				this.size++;
				return true;
			}
			curr = curr.getNext();
		}
		node = new Node<E>(obj, curr.getPrev(), curr);
		node.getPrev().setNext(node);
		node.getNext().setPrev(node);
		this.size++;
		return true;
	}

	// Returns size of list
	@Override
	public int size() {
		return this.size;
	}

	// Removes the first occurrence of obj from the list
	// Returns true if erased; false otherwise
	@Override
	public boolean remove(E obj) {
		if(this.isEmpty()) {
			return false;
		}
		else {
			Node<E> ntr = this.header.getNext();
			while(ntr != this.header) {
				if(ntr.getElement().equals(obj)) { // Finds element obj in list
					ntr.getPrev().setNext(ntr.getNext());
					ntr.getNext().setPrev(ntr.getPrev());
					ntr.cleanLinks();
					this.size--;
					return true;
				}
				ntr = ntr.getNext();
			}
			return false;
		}
	}

	// Removes element at position index
	// Returns true if the element is erased
	@Override
	public boolean remove(int index) {
		if(index < 0 || index > this.size()-1) {  
			throw new IndexOutOfBoundsException("Invalid index: " + index);
		}
		this.remove(this.get(index));
		return true;
	}

	// Removes all occurrences of obj
	// Returns number of copies removed
	@Override
	public int removeAll(E obj) {
		int count = 0;
		Node<E> curr = this.header.getNext();
		while(curr != this.header) {
			remove(obj);
			count++;
			curr = curr.getNext();
		}
		return count;
	}

	// Returns first element in list; null if empty
	@Override
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getNext().getElement();
	}

	// Returns last element in list; null if empty
	@Override
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getPrev().getElement();
	}

	// Returns element at given index
	@Override
	public E get(int index) {
		int count = 0;
		Node<E> curr = this.header.getNext();

		if(index < 0 || index > size()-1) { 
			throw new IndexOutOfBoundsException("Invalid index: " + index);
		}
		while(count != index) {
			curr = curr.getNext();
			count++;
		}
		return curr.getElement();
	}

	// Removes everything
	@Override
	public void clear() {
		int length = this.size();
		for(int i = 0; i < length; ++i) {
			this.remove(0);
		}
	}

	// Returns true if element is present; false otherwise
	@Override
	public boolean contains(E e) {
		if(this.isEmpty()) {
			return false;
		}
		else {
			Node<E> curr = this.header.getNext();
			while(curr != this.header) {
				if(curr.getElement().equals(e)) {
					return true;
				}
				curr = curr.getNext();
			}
		}
		return false;
	}

	// Returns true if list is empty; false otherwise
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	// Returns the index of the first occurrence of element; -1 if not present
	@Override
	public int firstIndex(E e) {
		Node<E> curr = this.header.getNext();
		if(this.isEmpty()) {
			return -1;
		}
		for(int i = 0; i < this.size(); ++i) {
			if(curr.getElement().equals(e)) {
				return i;
			}
			curr = curr.getNext();
		}
		return -1;
	}

	// Returns the index of the last occurrence of element; -1 if not present
	@Override
	public int lastIndex(E e) {
		Node<E> curr = this.header.getPrev();
		if(this.isEmpty()) {
			return -1;
		}
		for(int i = this.size()-1; i >= 0; --i) {
			if(curr.getElement().equals(e)) {
				return i;
			}
			curr = curr.getPrev();
		}
		return -1;
	}

	// Inner class Node
	private static class Node<E> {

		// Instance Variables
		private E element;
		private Node<E> prev, next;

		// Constructors
//		public Node() {}

		public Node(E element, Node<E> prev, Node<E> next) {
			this.element = element;
			this.prev = prev;
			this.next = next;
		}

		// Getters and Setters
		public Node<E> getPrev() {
			return this.prev;
		}

		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		public Node<E> getNext(){
			return this.next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}

		public E getElement() {
			return this.element;
		}

		// Helper method
		public void cleanLinks() {
			this.prev = null;
			this.next = null;
			this.element = null;
		}
	}

	// Inner class Iterator
	private class MyIterator implements Iterator<E>{

		// Instance Variable
		Node<E> curr;

		// Constructor
		public MyIterator() {
			curr = header.getNext();
		}

		@Override
		public boolean hasNext() {
			return curr.getNext() != header;
		}

		@Override
		public E next() {
			if(!hasNext()) {
				throw new NoSuchElementException("There is no next element.");
			}
			E node = curr.getNext().getElement();
			curr = curr.getNext();
			return node;
		}
	} 

}
