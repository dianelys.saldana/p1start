package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;       

@Path("/cars")
public class CarManager {

	// Instance variable
	private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

	// Returns an array with all the cars in the list
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] newList = new Car[cList.size()];
		for(int i = 0; i < cList.size(); ++i) {
			newList[i] = cList.get(i);
		}
		return newList;
	}

	// Returns the car that matches the given id parameter if found
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for(int i = 0; i < cList.size(); ++i) {
			if(cList.get(i).getCarId() == id) {
				return cList.get(i);
			}
		}
		 throw new WebApplicationException(404);
	} 

	// Adds a new car to the list if its id is not already there
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		if(car.getCarBrand() == null || car.getCarId() < 0 || car.getCarModel() == null || car.getCarPrice() < 0) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		for(int i = 0; i < cList.size(); ++i) {
			if(car.getCarId() == cList.get(i).getCarId()) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
		}
		cList.add(car);
		return Response.status(201).build();
	}  

	// Updates old car information with the new given in the parameter if found in the list
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		if(car.getCarBrand() == null || car.getCarId() < 0 || car.getCarModel() == null || car.getCarPrice() < 0) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		for(int i = 0; i < cList.size(); ++i) {
			if(car.getCarId() == cList.get(i).getCarId()) {
				cList.remove(i);
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}   

	// Deletes car if the id given in the parameter is found
	@DELETE
	@Path("{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for(int i = 0; i < cList.size(); ++i) {
			if(cList.get(i).getCarId() == id) {
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	} 

}